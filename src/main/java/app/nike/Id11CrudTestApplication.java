package app.nike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Id11CrudTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Id11CrudTestApplication.class, args);
	}

}
